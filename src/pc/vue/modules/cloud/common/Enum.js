/*
 * @Descripttion:
 * @version:
 * @Author: qinyonglian
 * @Date: 2019-11-04 14:01:21
 * @LastEditors: qinyonglian
 * @LastEditTime: 2019-11-04 15:32:28
 */

// import { isPlainObject } from '@/helper/util';

class Enum {
  constructor(eNum) {
    // 关于isPlainObject方法没有找到 需要咨询一下梓钦
    // if (!isPlainObject(eNum) || eNum === null) {
    //   throw new Error('参数异常');
    // }
    this.init(eNum);
  }

  /*
   * 遍历枚举值
   * @param eNum
   */
  init(eNum) {
    const keys = Object.keys(eNum);
    keys.forEach((k) => {
      const val = eNum[k];
      this[this[k] = val] = k;
    });
  }

  /*
   * 判断是否存在某一个字典项
   * @param {}} key
   */
  includeOf(key) {
    return !!this[key];
  }
}

export default function createEnum(eNum) {
  return new Enum(eNum);
}
