/*
 * @Author: Qinyonglian
 * @Date: 2019-08-26 18:35:15
 * @LastEditors: Qinyonglian
 * @LastEditTime: 2019-09-02 13:21:10
 */
import Vue from 'vue';

const EventBus = new Vue();
export { EventBus };
