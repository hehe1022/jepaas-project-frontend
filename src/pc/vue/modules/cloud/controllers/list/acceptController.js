/* eslint-disable class-methods-use-this */
/*
 * @Descripttion:
 * @Author: qinyonglian
 * @Date: 2019-11-11 15:39:57
 * @LastEditors: qinyonglian
 * @LastEditTime: 2019-11-21 17:02:57
 */

// 传输历史数据模型
import AcceptModel from '../../model/acceptModel';
import {
  sendList,
  reciveList,
  shareDelete,
  shareTransfer,
  shareFilesTransfer,
} from '../../actions/action';

export default class AcceptList {
  /* 发出分享列表接口
   * @param {key} 搜索关键字
   * @param {order} 排序条件[{code:"time||size",type:"desc||asc"}]
   * @return:
   */

  async sendShareList(key, order) {
    // 1.调取接口，渲染列表
    // 2.数据模型 {
    const res = await sendList({
      key,
      order: JSON.stringify(order),
    });
    return res.map(item => AcceptModel.create(item));
  }

  /* 接受分享列表接口
   * @param {key} 搜索关键字
   * @param {order} 排序条件[{code:"time||size",type:"desc||asc"}]
   * @return:
   */
  async reciveShareList(key, order) {
    // 1.调取接口，渲染列表
    // 2.数据模型 {
    const res = await reciveList({
      key,
      order: JSON.stringify(order),
    });
    return res.map(item => AcceptModel.create(item));
  }

  /* 删除分享记录列表
   * @param {shareId} 删除记录的id
   * @return:
   */
  async shareDeleteList(shareIds) {
    const res = await shareDelete({
      shareIds: shareIds.join(','),
    });
    return res;
  }

  /**
   * 分享的转存
   * @param { 分享记录的ID } shareIds
   * @param { 转存目录的id } targetNodeId
   * @memberof AcceptList
   */
  async shareDocTransfer(shareIds, targetNodeId, diskType) {
    const res = await shareFilesTransfer({
      shareIds,
      targetNodeId,
      diskType,
    });
    return res;
  }

  /* 分享文件
   * @param { shareId } 分享记录的id
   * @param { targetNodeId } 转存目录的id
   * @return:
   */
  static async shareFileTransfer(shareIds, targetNodeId) {
    const res = await shareTransfer({
      shareIds,
      targetNodeId,
    });
    return res;
  }


  static create() {
    return new AcceptList(...arguments);
  }
}
