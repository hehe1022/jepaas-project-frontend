/* eslint-disable new-cap */
/*
 * @Descripttion:
 * @version:
 * @Author: qinyonglian
 * @Date: 2019-11-04 14:52:24
 * @LastEditors: qinyonglian
 * @LastEditTime: 2019-11-20 17:06:15
 */
import { getfilesize } from '../common/util';

export default class recoveryModel {
  constructor(option) {
    this._init(option);
  }

  _init(option) {
    // 默认没有被选中
    this.checked = false;
    this.name = option.name;
    this.UUID = option.UUID;
    this.icon = option.icon;
    this.uploadTime = option.uploadTime;
    // 移动到item上的颜色
    this.backgroundCheck = false;
    this.docSrc = option.docSrc; // 上传的目录
    this.uploadStatus = option.uploadStatus; // 上传的进度
    this.uploadIssue = option.uploadIssue; // 上传是否失败(可能重复上传文件了等报错信息)
    this.checkTag = false;
    this.size = getfilesize(option.size) || null;
    this.docFunc = [
      { icon: 'jeicon-trash-o', clickText: 'remove' },
    ];
    this.uploadData = true; // 证明该数据数正在上传的数据
  }

  /*
   * 创建对象
   */
  static create(options) {
    return new recoveryModel(options);
  }
}
