import { roleCompanyData, roleTitleScope } from '../common/util';

export default class roleModel {
  constructor(option) {
    this._init(option);
  }

  _init(option) {
    this.setData = [
      {
        text: '可管理，编辑文件', jurisdiction: true, data: roleCompanyData(option, 'manage'), showMain: false, role: 'manage', checkTitle: roleTitleScope(option, 'manage'),
      },
      {
        text: '可编辑文件', jurisdiction: true, data: roleCompanyData(option, 'edit'), showMain: false, role: 'edit', checkTitle: roleTitleScope(option, 'edit'),
      },
      {
        text: '仅可查看、下载文件', jurisdiction: true, data: roleCompanyData(option, 'download'), showMain: false, role: 'download', checkTitle: roleTitleScope(option, 'download'),
      },
      {
        text: '仅可查看文件', jurisdiction: true, data: roleCompanyData(option, 'check'), showMain: false, role: 'check', checkTitle: roleTitleScope(option, 'check'),
      },
    ];
  }

  /*
   * 创建对象
   */
  static create(options) {
    return new roleModel(options);
  }
}
