/*
 * @Descripttion:
 * @Author: qinyonglian
 * @Date: 2020-03-23 14:47:07
 * @LastEditors: qinyonglian
 * @LastEditTime: 2020-03-23 15:52:28
 */
import {install} from './../../install.js'
import index from './index.vue'

//安装组件
install('cloud', index);
