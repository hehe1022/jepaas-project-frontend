/*
 * @Descripttion:
 * @version:
 * @Author: qinyonglian
 * @Date: 2019-11-07 09:51:14
 * @LastEditors: Shuangshuang Song
 * @LastEditTime: 2020-12-17 16:09:10
 */

import fetch from '../../../static/js/common';
import { downLoadScript } from '../common/util';
import {
  DOC_PREVIEW,
  RECOVERY_RECYCLE_BIN,
  EMPTY_RECYCLE_BIN,
  FILE_MOVE,
  FILE_COPY,
  SKYDRIVE_DOWNLOAD,
  IS_SKYDRIVE_DOWNLOAD,
  SHARE_TRANSFER,
  FOLDER_QUERY,
  FOLDER_LIST,
  QUERY_RECYCLE_BIN,
  SEND_SHARE_LIST,
  RECIVE_SHARE_LIST,
  TRANSFER_LIST_RECORD,
  FILE_RECYCLE,
  FILE_ADMIN,
  FILE_RENAME,
  SHAREING_FILES,
  SHARE_TARGET,
  ADD_TAGS,
  DELETE_RECORD,
  SHARE_DELETE,
  CREATE_FOLDER,
  MICRO_MAIL,
  LOAD_TREE,
  SET_FILE,
  DISK_LOG,
  POWER_FILE,
  UPLOAD_FILES,
} from './api';
// 获取拖拽文件夹上传的权限
export function getUpLoadFiles(param) {
  return fetch(UPLOAD_FILES, {}, {
    type: 'post',
    isFormSubmit: true,
    data: param,
    contentType: 'application/x-www-form-urlencoded',
  })
    .then(data => data)
    .catch();
}

// 获取文件的权限
export function getPowerFile(param) {
  return fetch(POWER_FILE, {}, {
    type: 'post',
    isFormSubmit: true,
    data: param,
    contentType: 'application/x-www-form-urlencoded',
  })
    .then((data) => {
      if (data.success) {
        return data.obj || [];
      }
      JE.msg(data.message);
      return false;
    })
    .catch();
}
// 获取操作的记录
export function getDiskLog(param) {
  return fetch(DISK_LOG, {}, {
    type: 'post',
    isFormSubmit: true,
    data: param,
    contentType: 'application/x-www-form-urlencoded',
  })
    .then((data) => {
      if (data.success) {
        return data.obj || [];
      }
      JE.msg(data.message);
      return false;
    })
    .catch();
}
// 设置文件的权限
export function setFile(param) {
  return fetch(SET_FILE, {}, {
    type: 'post',
    isFormSubmit: true,
    data: param,
    contentType: 'application/x-www-form-urlencoded',
  })
    .then((data) => {
      if (data.success) {
        // JE.msg(data.message);
        return true;
      }
      JE.msg(data.message);
      return false;
    })
    .catch();
}
// 致微邮
export function loadTree(param) {
  return fetch(LOAD_TREE, {}, {
    type: 'post',
    isFormSubmit: true,
    data: param,
    contentType: 'application/x-www-form-urlencoded',
  })
    .then((data) => {
      if (data.success) {
        return data.obj || {};
      }
      JE.msg(data.message);
      return false;
    })
    .catch();
}

// 致微邮
export function goMail(param) {
  return fetch(MICRO_MAIL, {}, {
    type: 'post',
    isFormSubmit: true,
    data: param,
    contentType: 'application/x-www-form-urlencoded',
  })
    .then((data) => {
      if (data.success) {
        return data.obj || {};
      }
      JE.msg(data.message);
      return false;
    })
    .catch();
}


/*
 * 创建文件夹
 */
export function folderByCreate(param) {
  return fetch(CREATE_FOLDER, {}, {
    type: 'post',
    isFormSubmit: true,
    data: param,
    contentType: 'application/x-www-form-urlencoded',
  })
    .then((data) => {
      if (data.success) {
        return data.obj || {};
      }
      JE.msg(data.message);
      return false;
    })
    .catch();
}

/*
 * 恢复回收站
 */
export function recoveryRecycleBin(param) {
  return fetch(RECOVERY_RECYCLE_BIN, {}, {
    type: 'post',
    isFormSubmit: true,
    data: param,
    contentType: 'application/x-www-form-urlencoded',
  })
    .then((data) => {
      if (data.success) {
        return data.obj || {};
      }
      return {};
    })
    .catch();
}
/*
 * 编辑标签
 */
export function editorTags(param) {
  return fetch(ADD_TAGS, {}, {
    type: 'post',
    isFormSubmit: true,
    data: param,
    contentType: 'application/x-www-form-urlencoded',
  })
    .then((data) => {
      if (data.success) {
        return true;
      }
      return false;
    })
    .catch();
}

/*
 * 清除回收站
 */

export function emptyRecycleBin(param) {
  return fetch(EMPTY_RECYCLE_BIN, {}, {
    type: 'post',
    isFormSubmit: true,
    data: param,
    contentType: 'application/x-www-form-urlencoded',
  })
    .then((data) => {
      if (data.success) {
        return true;
      }
      JE.msg(data.message);
      return false;
    })
    .catch();
}

/*
 * 文件移动
 */
export function fileMove(param) {
  return fetch(FILE_MOVE, {}, {
    type: 'post',
    isFormSubmit: true,
    data: param,
    contentType: 'application/x-www-form-urlencoded',
  })
    .then((data) => {
      if (data.success) {
        JE.msg(data.message);
        return data.obj || [];
      }
      JE.msg(data.message);
      return false;
    })
    .catch();
}

/*
 * 文件复制
 */
export function fileCopy(param) {
  return fetch(FILE_COPY, {}, {
    type: 'post',
    isFormSubmit: true,
    data: param,
    contentType: 'application/x-www-form-urlencoded',
  })
    .then((data) => {
      if (data.success) {
        JE.msg(data.message);
        return data.obj || [];
      }
      JE.msg(data.message);
      return false;
    })
    .catch();
}

/*
 * 网盘下载
 */
export function skydriveDownload(param) {
  const token = JE.getCookies('authorization');
  fetch(IS_SKYDRIVE_DOWNLOAD, {}, {
    type: 'post',
    // isFormSubmit: true,
    data: param,
    contentType: 'application/x-www-form-urlencoded',
  }).then((res) => {
    !res.success && JE.msg('空文件夹不支持下载!');
    res.success && downLoadScript(`${SKYDRIVE_DOWNLOAD}?authorization=${token}&nodeIds=${param.nodeIds}&diskType=${param.diskType}`);
    return true;
  });
}

// 网盘判断是否是文档管理员

export function isFileAdmin() {
  return fetch(FILE_ADMIN, {}, {
    type: 'post',
    // isFormSubmit: true,
    data: '',
    contentType: 'application/x-www-form-urlencoded',
  });
}
/*
 * 文件预览功能
 *
 * @export
 * @param { 预览的参数 nodeId,diskType} param
 */
export function getPreview(param) {
  return fetch(DOC_PREVIEW, {}, {
    type: 'get',
    // isFormSubmit: true,
    data: param,
    contentType: 'application/x-www-form-urlencoded',
  });
}

/*
 * 分享文件
 */
export function shareTransfer(param) {
  return fetch(SHARE_TRANSFER, {
    params: param,
  }, {
    async: true,
  })
    .then(data => data)
    .catch();
}

/*
 * 网盘上传
 */
// export function skyDriveDownLoad(param) {
//   return fetch(SKYDRIVE_UPLOAD, {}, {
//     type: 'post',
//     data: param,
//     contentType: 'multipart/form-data',
//   })
//     .then((data) => {
//       if (data.success) {
//         return data.obj || [];
//       }
//       JE.msg(data.message);
//       return [];
//     })
//     .catch();
// }


/*
 * 目录搜索接口
 */
export function floderQuery(param) {
  return fetch(FOLDER_QUERY, {}, {
    type: 'post',
    isFormSubmit: true,
    data: param,
    contentType: 'application/x-www-form-urlencoded',
  })
    .then((data) => {
      if (data.success) {
        return data.obj || [];
      }
      return JE.msg(data.message);
    })
    .catch();
}


/*
 * 文件列表接口
 */
export function floderList(param) {
  return fetch(FOLDER_LIST, {}, {
    type: 'post',
    isFormSubmit: true,
    data: param,
    contentType: 'application/x-www-form-urlencoded',
  })
    .then((data) => {
      if (data.success) {
        return data.obj || [];
      }
      JE.msg(data.message);
      return false;
    })
    .catch();
}

/*
 * 查询回收站接口
 */
export function queryRecycleBin(param) {
  return fetch(QUERY_RECYCLE_BIN, {}, {
    type: 'post',
    isFormSubmit: true,
    data: param,
    contentType: 'application/x-www-form-urlencoded',
  })
    .then((data) => {
      if (data.success) {
        return data.obj || [];
      }
      JE.msg(data.message);
      return [];
    })
    .catch();
}

/*
 * 发出去分享列表
 */
export function sendList(param) {
  return fetch(SEND_SHARE_LIST, {}, {
    type: 'post',
    isFormSubmit: true,
    data: param,
    contentType: 'application/x-www-form-urlencoded',
  })
    .then((data) => {
      if (data.success) {
        return data.obj || [];
      }
      JE.msg(data.message);
      return [];
    })
    .catch();
}

/*
 * 收到分享列表
 */
export function reciveList(param) {
  return fetch(RECIVE_SHARE_LIST, {}, {
    type: 'post',
    isFormSubmit: true,
    data: param,
    contentType: 'application/x-www-form-urlencoded',
  })
    .then((data) => {
      if (data.success) {
        return data.obj || [];
      }
      JE.msg(data.message);
      return [];
    })
    .catch();
}

/*
 * 分享列表删除
 */
export function shareDelete(param) {
  return fetch(SHARE_DELETE, {}, {
    type: 'post',
    isFormSubmit: true,
    data: param,
    contentType: 'application/x-www-form-urlencoded',
  })

    .then((data) => {
      if (data.success) {
        return data.obj || {};
      }
      JE.msg(data.message);
      return {};
    })
    .catch();
}

/*
 * 传输列表记录查询
 */
export function shareListRecord(param) {
  return fetch(TRANSFER_LIST_RECORD, {}, {
    type: 'post',
    isFormSubmit: true,
    data: param,
    contentType: 'application/x-www-form-urlencoded',
  })
    .then((data) => {
      if (data.success) {
        return data.obj || [];
      }
      JE.msg(data.message);
      return [];
    })
    .catch();
}


/*
 * 文件放入回收站
 */
export function flieRecycle(param) {
  return fetch(FILE_RECYCLE, {}, {
    type: 'post',
    isFormSubmit: true,
    data: param,
    contentType: 'application/x-www-form-urlencoded',
  })
    .then((data) => {
      if (data.success) {
        return true;
      }
      JE.msg(data.message);
      return false;
    })
    .catch();
}

/*
 * 目录/文件重命名
 */
export function fileRename(param) {
  return fetch(FILE_RENAME, {}, {
    type: 'post',
    isFormSubmit: true,
    data: param,
    contentType: 'application/x-www-form-urlencoded',
  })
    .then((data) => {
      if (data.success) {
        JE.msg(data.message);
        return true;
      }
      JE.msg(data.message);
      return false;
    })
    .catch();
}


/*
 * 分享文件
 */
export function shareingFiles(param) {
  return fetch(SHAREING_FILES, {}, {
    type: 'post',
    isFormSubmit: true,
    data: param,
    contentType: 'application/x-www-form-urlencoded',
  })
    .then((data) => {
      if (data.success) {
        JE.msg(data.obj);
        return true;
      }
      JE.msg(data.message);
      return false;
    })
    .catch();
}

/*
 * 分享文件
 */
export function shareFilesTransfer(param) {
  return fetch(SHARE_TARGET, {}, {
    type: 'post',
    isFormSubmit: true,
    data: param,
    contentType: 'application/x-www-form-urlencoded',
  })
    .then((data) => {
      if (data.success) {
        return data;
      }
      return false;
    })
    .catch();
}

/*
 * 删除传输历史记录
 */
export function deleteRecords(param) {
  return fetch(DELETE_RECORD, {}, {
    type: 'post',
    isFormSubmit: true,
    data: param,
    contentType: 'application/x-www-form-urlencoded',
  })
    .then((data) => {
      if (data.success) {
        return true;
      }
      JE.msg(data.message);
      return false;
    })
    .catch();
}
