import { install } from '../../install.js';
import index from './index.vue';
// 加载依赖平台信息
JE.loadChartScript(true);
// 编辑器
const js = [
  '/static/ux/moment/moment.min.js',
];
JE.loadScript(js);
// 安装组件
install('worktask', index);
/**惠博普OA 工作待办   办公系统-->我的工作-->流程工作台 */
