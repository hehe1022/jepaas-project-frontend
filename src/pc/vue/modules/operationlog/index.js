/*
 * @Descripttion: 简要描述 ——
 * @Author: 宋爽爽
 * @Date: 2020-03-30 10:02:25
 * @LastEditTime: 2020-03-30 16:07:38
 */
import { install } from '../../install.js';
import OperationLog from './index.vue';

// 加载依赖平台信息
JE.loadChartScript(true);
// 安装组件
install('operationlog', OperationLog);
