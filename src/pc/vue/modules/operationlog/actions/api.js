/*
 * @Descripttion: 描述
 * @version: V1.0.0
 * @Author: Shuangshuang Song
 * @LastEditors: Shuangshuang Song
 * @Date: 2020-05-23 12:34:52
 * @LastEditTime: 2020-06-15 10:13:52
 */
import fetch from '../../../static/js/common';

/**
 * 获取菜单集合
 * @param param
 * @returns {Promise<T | never>}
 */
export function getMenuInfo(param) {
  return fetch('/je/portal/developPortal/getMenuInfo', {}, {
    type: 'post',
    contentType: 'application/x-www-form-urlencoded',
    data: param,
    isFormSubmit: true,
  })
    .then(data => data)
    .catch();
}

/**
 * 获取操作人员信息
 * @param param
 * @returns {Promise<T | never>}
 */
export function getDevelopUsers(param) {
  return fetch('/je/portal/developPortal/getDevelopUsers', {}, {
    type: 'post',
    contentType: 'application/x-www-form-urlencoded',
    data: param,
    isFormSubmit: true,
  })
    .then(data => data)
    .catch();
}

/**
 * 获取操作日志
 * @param param
 * @returns {Promise<T | never>}
 */
export function loadDevelopUserLog(param) {
  return fetch('/je/portal/developPortal/loadDevelopUserLog ', {}, {
    type: 'post',
    contentType: 'application/x-www-form-urlencoded',
    data: param,
    isFormSubmit: true,
  })
    .then(data => data)
    .catch();
}
/**
 * 获取远程获取license
 * @param param
 * @returns {Promise<T | never>}
 */
 export function getLicense(param) {
  return fetch('/je/sysConfig/sysMode/offLineAuthorize', {}, {
    type: 'post',
    contentType: 'application/x-www-form-urlencoded',
    data: param,
    isFormSubmit: true,
  })
    .then(data => data)
    .catch();
}

/**
 * 本地授权
 * @param param
 * @returns {Promise<T | never>}
 */
export function onlineAuthorize(param) {
  return fetch('/je/sysConfig/sysMode/onlineAuthorize', {}, {
    type: 'post',
    contentType: 'application/x-www-form-urlencoded',
    data: param,
    isFormSubmit: true,
  })
    .then(data => data)
    .catch();
}

/**
 * 获取基本信息
 * @param param
 * @returns {Promise<T | never>}
 */
export function getAuthInfo(param) {
  return fetch('/je/sysConfig/sysMode/getAuthInfo', {}, {
    type: 'post',
    contentType: 'application/x-www-form-urlencoded',
    data: param,
    isFormSubmit: true,
  })
    .then(data => data)
    .catch();
}

/**
 * 离线授权
 * @param param
 * @returns {Promise<T | never>}
 */
export function offLineAuthorize(param) {
  return fetch('/je/sysConfig/sysMode/offLineAuthorize', {}, {
    type: 'post',
    contentType: 'application/x-www-form-urlencoded',
    data: param,
    isFormSubmit: true,
  })
    .then(data => data)
    .catch();
}

