const spe = {
  '微信': 'WeChat',
  '钉钉': 'DingTalk',
  '轻云蒜瓣': '轻云蒜瓣',
  '账号/手机': 'Account/Cell Phone',
  '密码格式不正确，请输入6位以上字母+数字': 'The password format is incorrect. Please enter more than 6 letters + numbers',
  '验证码不存在!': 'Verification code does not exist!',
  '该手机号未注册!': 'The mobile phone number is not registered!',
  '请输入密码!': 'Please input the password',
  '请输入密码': 'Please input the password',
  '登录账号不能为空，请重新输入': 'The login account cannot be empty. Please re-enter it.',
  '请输入手机号/账户': 'Please enter your mobile number/account',
  '请将滑块拖动到右侧': 'Drag the slider to the right',
  '请输入短信验证码': 'Please enter SMS Verification Code',
  '两次输入密码不一致': 'Two inconsistencies in password input',
  '请先滑动解锁': 'Slide and unlock first',
  '验证通过': 'Verifying and passing',
  '请输入账号': 'Please enter your account number',
  '手机号格式不正确，需重新输入': 'The format of mobile phone number is incorrect. It needs to be re-entered'
};
export default spe;