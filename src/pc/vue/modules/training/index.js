/*
 * @Descripttion: 
 * @Author: qinyonglian
 * @Date: 2021-02-07 14:40:14
 * @LastEditors: qinyonglian
 * @LastEditTime: 2021-02-07 15:24:04
 */
// import 'babel-polyfill';
import { install } from '../../install.js';
import Training from './index.vue';

// 加载依赖平台信息
// JE.loadChartScript(true);
// 安装组件
install('Training', Training);

// 安装组件
// install('trainging', trainging, () => {
//   // 自动初始化组建
//   const loginDom = document.createElement('div');
//   loginDom.innerHTML = '<trainging></trainging>';
//   document.body.appendChild(loginDom);
//   new Vue({
//     el: trainging,
//   });
// });
