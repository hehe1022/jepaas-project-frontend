
const HOST_BASIC = '';

// 获取微邮未读数量
export const POST_GET_NO_READ_COUNT = `${HOST_BASIC}/je/micromail/getNoReadCount`;
// 获取微邮 全部数量 getMicromailTotalCount
export const POST_GET_MICROMAIL_TOTAL_COUNT = `${HOST_BASIC}/je/micromail/getMicromailTotalCount`;
// 获取微邮  各自不同tab的总数 getMicromailTotalTypeCount
export const POST_GET_MICROMAIL_TOTAL_TYPE_COUNT = `${HOST_BASIC}/je/micromail/getMicromailTotalTypeCount`;
// 获取微邮
export const POST_GET_MICROMAIL_BY_ID = `${HOST_BASIC}/je/micromail/getMicromailById`;
// 获取微邮已赞数据列表
export const POST_GET_MICROMAIL_UPS_BY_ID = `${HOST_BASIC}/je/micromail/getMicromailUpsById`;
// 获取已阅列表
export const POST_GET_READ_MICROMAIL_LIST = `${HOST_BASIC}/je/micromail/getReadMicromailList`;
// 获取未阅列表
export const POST_GET_NO_READ_MICROMAIL_LIST = `${HOST_BASIC}/je/micromail/getNoReadMicromailList`;
// 获取评论列表
export const POST_GET_MICROMAIL_COMMENTS = `${HOST_BASIC}/je/micromail/getMicromailComments`;
// 发送未阅提醒
export const POST_TO_MICROMAIL_REMIND = `${HOST_BASIC}/je/micromail/toMicromailRemind`;
// 获取我的微邮
export const POST_GET_MY_MICROMAIL_LISTS = `${HOST_BASIC}/je/micromail/getMyMicromailLists`;
// 创建我的微邮
export const POST_CREATE_MICROMAIL = `${HOST_BASIC}/je/micromail/createMicromail`;
// 修改我的微邮
export const POST_UPDATE_MICROMAIL = `${HOST_BASIC}/je/micromail/updateMicromail`;
// 删除微邮
export const POST_DELETE_MICROMAIL = `${HOST_BASIC}/je/micromail/deleteMicromail`;
// 评论微邮
export const POST_CREATE_COMMENT = `${HOST_BASIC}/je/micromail/createComment`;
// 删除评论
export const POST_DELETE_COMMENT = `${HOST_BASIC}/je/micromail/deleteComment`;
// 添加微邮阅读记录
export const POST_ADD_READ_MICROMAIL = `${HOST_BASIC}/je/micromail/addReadMicromail`;
