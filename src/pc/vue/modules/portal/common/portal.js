import { fetchAsync } from './fetch';
import api from './api';
import {
  getImg, dateFormat4Time, isShowTime, setListTime,
} from './tools';

/**
 * 请求门户
 * @param {*} userId 登陆人员ID
 * @returns {Promise<T | never>}
 */

export function fetchPageData(param) {
  return fetchAsync(api.POST_PAGE, {}, {
    type: 'post',
    isFormSubmit: true,
    data: param,
    contentType: 'application/x-www-form-urlencoded',
  })
    .then((data) => {
      if (data.totalCount) {
        const mhData = [];
        if (data.rows.length) {
          data.rows.map((itemData) => {
            mhData.push({ mhId: itemData.JE_MHXW_MH_ID });
          });
          return mhData;
        } return [];
      }
      return [];
    })
    .catch();
}


/**
 *一级模块
 * @param param
 * @param {*} JE_MHXW_MH_ID   门户ID
 * @returns {Promise<T | never>}
 */
export function fetchFirstLeveRoute(param) {
  return fetchAsync(api.POST_LEVEL_ONE, {}, {
    type: 'post',
    isFormSubmit: true,
    data: param,
    contentType: 'application/x-www-form-urlencoded',
  })
    .then((data) => {
      const newData = [];
      if (data.rows.length) {
        data.rows.map((itemData) => {
          newData.push({
            text: itemData.BK_NAME,
            bkId: itemData.JE_MHXW_BK_ID,
            mhId: itemData.JE_MHXW_MH_ID,
            ejbk: itemData.ejbk,
          });
        });
        return newData;
      }
      return [];
    })
    .catch();
}


/**
 *二级模块
 * @param param
 * @param {*} JE_MHXW_MH_ID   门户ID
 * @param {*} SY_PARENT         一级板块ID
 * @returns {Promise<T | never>}
*/
// export function fetchTwoLeveRoute(param) {
//   return fetchAsync(api.POST_LEVEL_SEC, {}, {
//     type: 'post',
//     isFormSubmit: true,
//     data: param,
//     contentType: 'application/x-www-form-urlencoded',
//   })
//     .then((data) => {
//       if (data.success) {
//         const newData = [];
//         if (data.obj.length) {
//           data.obj.map((itemData) => {
//             newData.push({
//               text: itemData.values.BK_NAME,
//               bkId: itemData.values.JE_MHXW_BK_ID,
//               mhId: itemData.values.JE_MHXW_MH_ID,
//             });
//           });
//           return newData;
//         } return [];
//       }
//       return [];
//     })
//     .catch();
// }
/**
 *轮播图
 * @param param
 * @param {*} JE_MHXW_MH_ID   门户id
 * @returns {Promise<T | never>}
*/
export function fetchCarousel(param) {
  return fetchAsync(api.POST_CAROUSEL, {}, {
    type: 'post',
    isFormSubmit: true,
    data: param,
    contentType: 'application/x-www-form-urlencoded',
  })
    .then((data) => {
      if (data.success) {
        if (data.obj.length) {
          const newData = [];
          data.obj.map((itemData) => {
            newData.push({
              title: itemData.values.XW_BT,
              imgId: itemData.values.JE_MHXW_XW_ID,
              mhId: itemData.values.JE_MHXW_MH_ID,
              src: getImg(true, itemData.values.XW_LBT, true),
              newsId: itemData.values.JE_MHXW_XW_ID,
            });
          });
          return newData;
        }
        return [];
      }
      return [];
    })
    .catch();
}
/**
 *查找新闻列表
 * @param param
 * @param {*} JE_MHXW_MH_ID   门户id
 * @returns {Promise<T | never>}
*/
export function fetchSearchNews(param) {
  return fetchAsync(api.POST_SEARCHNEWS, {}, {
    type: 'post',
    isFormSubmit: true,
    data: param,
    contentType: 'application/x-www-form-urlencoded',
  })
    .then((data) => {
      if (data.totalCount) {
        if (data.rows.length) {
          const newData = [];
          data.rows.map((itemData) => {
            newData.push({
              dzsl: itemData.XW_DZSL, // 点赞
              ydsl: itemData.XW_YDS,
              userName: itemData.SY_CREATEUSERNAME, // 新闻创建者名称
              bt: itemData.XW_BT, // 摘要
              isZd: itemData.XW_SFZD == '1', // 置顶
              type: itemData.XW_TPFS_CODE, // 展示方式
              plsl: itemData.XW_PLS, // 评论数量
              time: setListTime(itemData.XW_SPTGSJ), // 时间
              newsId: itemData.JE_MHXW_XW_ID, // id,
              con: itemData.XW_ZY,
              bkname: itemData.XW_EJFL || itemData.XW_YJFL, // 新闻来源版块
              isYd: itemData.XW_SFYD,
              partName: itemData.SY_CREATEORGNAME,
              fj: itemData.XW_XWFJ ? JSON.parse(itemData.XW_XWFJ).length : 0,
              img: getImg((itemData.XW_TPFS_CODE != ('DT' || 'DTZY')), itemData.XW_TPFS_CODE == ('DT' || 'DTZY') ? itemData.XW_TP : itemData.XW_FJZD),
              turnTime: dateFormat4Time(itemData.XW_SPTGSJ),
              isShowTime: isShowTime(itemData.XW_SPTGSJ),
            });
          });
          return newData;
        } return [];
      }
      return [];
    })
    .catch();
}
/**
 *重点关注
 * @param param
 * @param {*} JE_MHXW_MH_ID   门户id
 * @returns {Promise<T | never>}
*/
export function fetchFocus(param) {
  return fetchAsync(api.POST_FOUSE, {}, {
    type: 'post',
    isFormSubmit: true,
    data: param,
    contentType: 'application/x-www-form-urlencoded',
  })
    .then((data) => {
      if (data.success) {
        if (data.obj.length) {
          const newData = [];
          data.obj.map((itemData) => {
            const setDate = (itemData.values.XW_SPTGSJ).substr(0, 10).replace('-', '年').replace('-', '月');
            newData.push({
              color: itemData.values.ZDGZGH_YS,
              newsTitle: itemData.values.XW_BT,
              dzsl: itemData.values.XW_DZSL,
              plsl: itemData.values.XW_PLS,
              ydsl: itemData.values.XW_YDS,
              time: `${setDate}日`,
              newsId: itemData.values.JE_MHXW_XW_ID,
              isDz: itemData.values.XW_SFDZ,
              isYd: itemData.values.XW_SFYD,
              userPhoto: JE.getUserPhoto(itemData.values.SY_CREATEUSERID, true),
            });
          });
          return newData;
        } return [];
      }
      return [];
    })
    .catch();
}
/**
 *新闻详情
 * @param param
 * @param {*} JE_MHXW_XW_ID  新闻id
 * @returns {Promise<T | never>}
*/
export function fetchNewsDetail(param) {
  return fetchAsync(api.POST_NEWSDETAIL, {}, {
    type: 'post',
    isFormSubmit: true,
    data: param,
    contentType: 'application/x-www-form-urlencoded',
  })
    .then((data) => {
      if (data.success) {
        const newsDetail = data.obj;
        return {
          fj: newsDetail.XW_XWFJ ? JSON.parse(newsDetail.XW_XWFJ) : '',
          newsTitle: newsDetail.XW_BT,
          userId: JE.getUserPhoto(newsDetail.SY_CREATEUSERID, true),
          userName: newsDetail.SY_CREATEUSERNAME,
          dzsl: newsDetail.XW_DZSL,
          type: newsDetail.XW_XWLX_CODE, // 展示方式
          newType: newsDetail.XW_XWLX_CODE == 'PDF', // pdf 还是其他
          plsl: newsDetail.XW_PLS, // 评论数量
          ydsl: newsDetail.XW_YDS,
          con: newsDetail.XW_XWLX_CODE == 'PDF' ? getImg(true, newsDetail.XW_PDFWJ) : newsDetail.XW_ZW,
          isDz: newsDetail.XW_SFDZ,
          XW_SFBM_CODE: newsDetail.XW_SFBM_CODE == '1',
          isYd: newsDetail.XW_SFYD,
          time: setListTime(newsDetail.XW_SPTGSJ),
          isZd: newsDetail.XW_SFZD,
          newsId: newsDetail.JE_MHXW_XW_ID,
          isOpenPl: newsDetail.XW_SFKQPL == '1',
          partName: newsDetail.SY_CREATEORGNAME, // newsDetail.SY_CREATEORGNAME,
          isOpenSY: newsDetail.XW_SFKQSY == '1',
        };
      }
      return {};
    })
    .catch();
}
/**
 *点赞
 * @param param
 * @param {*} JE_MHXW_XW_ID  新闻id
 * @returns {Promise<T | never>}
*/
export function fetchZan(param) {
  return fetchAsync(api.POST_DOZAN, {}, {
    type: 'post',
    isFormSubmit: true,
    data: param,
    contentType: 'application/x-www-form-urlencoded',
  })
    .then((data) => {
      if (data.success) {
        const newsDetail = data.obj;
        JE.msg(`${newsDetail.XW_SFDZ == 1 ? '' : '取消'}点赞成功!`);
        return {
          dzsl: newsDetail.XW_DZSL,
          newsId: newsDetail.JE_MHXW_XW_ID,
          isDz: newsDetail.XW_SFDZ,
        };
      }
      return {};
    })
    .catch();
}
/**
 *获取评论
 * @param param
 * @param {*} JE_MHXW_XW_ID 新闻ID
 * @param {*} start 起始位数
 * @param {*} limit 每页数量
 * @returns {Promise<T | never>}
*/
export function fetchComment(param) {
  return fetchAsync(api.POST_COMMENT, {}, {
    type: 'post',
    isFormSubmit: true,
    data: param,
    contentType: 'application/x-www-form-urlencoded',
  })
    .then((data) => {
      if (data.totalCount) {
        const commentData = [];
        data.rows.map((itemData) => {
          commentData.push({
            sendName: itemData.SY_CREATEUSERNAME,
            sendTime: itemData.SY_CREATETIME,
            sendId: JE.getUserPhoto(itemData.SY_CREATEUSERID, true),
            newsId: itemData.JE_MHXW_XW_ID,
            newsUserId: itemData.SY_CREATEUSERID,
            con: itemData.PL_PLNR,
            plId: itemData.JE_MHXW_PL_ID,
            isMe: itemData.SY_CREATEUSERID == JE.getCurrentUser().id,
          });
        });
        return commentData;
      } return [];
    })
    .catch();
}
/**
 *添加评论
 * @param param
 * @param {*} JE_MHXW_XW_ID  新闻id
 * @returns {Promise<T | never>}
*/
export function fetchAddComment(param) {
  return fetchAsync(api.POST_ADDCOMMENT, {}, {
    type: 'post',
    isFormSubmit: true,
    data: param,
    contentType: 'application/x-www-form-urlencoded',
  })
    .then((data) => {
      if (data.success) {
        const newsDetail = data.obj;
        JE.msg('添加评论成功！');
        return {
          sendName: newsDetail.SY_CREATEUSERNAME,
          sendTime: newsDetail.SY_CREATETIME,
          sendId: JE.getUserPhoto(newsDetail.SY_CREATEUSERID, true),
          newsId: newsDetail.JE_MHXW_XW_ID,
          newsUserId: newsDetail.SY_CREATEUSERID,
          con: newsDetail.PL_PLNR,
          plId: newsDetail.JE_MHXW_PL_ID,
          isMe: newsDetail.SY_CREATEUSERID == JE.getCurrentUser().id,
        };
      }
      return {};
    })
    .catch();
}
/**
 *删除评论
 * @param param
 * @param {*} JE_MHXW_XW_ID  新闻id
 * @param {*} JE_MHXW_PL_ID  评论id
 * @returns {Promise<T | never>}
*/
export function fetchDeleteComment(param) {
  return fetchAsync(api.POST_DELETECOMMENT, {}, {
    type: 'post',
    isFormSubmit: true,
    data: param,
    contentType: 'application/x-www-form-urlencoded',
  })
    .then((data) => {
      if (data.success) {
        JE.msg('删除评论成功！');
        return {
          plId: param.JE_MHXW_PL_ID,
        };
      }
      return {};
    })
    .catch();
}
/**
 *查询点赞数量
 * @param param
 * @param {*} JE_MHXW_XW_ID  新闻id
 * @returns {Promise<T | never>}
*/
export function fetchZanNum(param) {
  return fetchAsync(api.POST_ZANNUM, {}, {
    type: 'post',
    isFormSubmit: true,
    data: param,
    contentType: 'application/x-www-form-urlencoded',
  })
    .then((data) => {
      if (data.success) {
        const zanList = data.obj;
        const zanData = [];
        data.obj.map((itemData) => {
          zanData.push({
            zanName: itemData.values.SY_CREATEUSERNAME,
            zanTime: itemData.values.SY_CREATETIME,
            sendId: itemData.values.SY_CREATEUSERID,
            zanId: itemData.values.JE_MHXW_XW_ID,
            zanUserPhoto: JE.getUserPhoto(itemData.values.SY_CREATEUSERID, true),
          });
        });
        return zanData;
      }
      return [];
    })
    .catch();
}
/**
 *从详情页面回来需要再次请求页面上单个id的数据来达到刷新页面的目的
 * @param param
 * @param {*} JE_MHXW_XW_ID  新闻id
 * @returns {Promise<T | never>}
*/
export function fetchSingleNews(param) {
  return fetchAsync(api.POST_SINGLENEWS, {}, {
    type: 'post',
    isFormSubmit: true,
    data: param,
    contentType: 'application/x-www-form-urlencoded',
  })
    .then((data) => {
      if (data.success) {
        const singleData = data.obj;
        return {
          newsId: singleData.JE_MHXW_XW_ID,
          newsPlsl: singleData.XW_PLS,
          newsDzsl: singleData.XW_DZSL,
          newsYdsl: singleData.XW_YDS,
          newsIsyd: singleData.XW_SFYD == 1,
        };
      }
      return [];
    })
    .catch();
}
