/*
 * @Author: mengping Zhang
 * @Date: 2019-10-14
 * @LastEditors: mengping Zhang
 * @LastEditTime:2019-10-14
 */
import Vue from 'vue';

const EventBus = new Vue();
export { EventBus };
