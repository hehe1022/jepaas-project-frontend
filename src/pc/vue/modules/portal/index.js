/*
 * @Description:
 * @version: V1.0.0
 * @Author: Shuangshuang Song
 * @Date: 2020-08-20 14:10:27
 * @LastEditTime: 2020-10-29 15:17:20
 * @LastEditors: Shuangshuang Song
 */
import { install } from '../../install.js';
import index from './index.vue';

// 加载依赖平台信息
/* JE.loadScript([
  '/static/ux/moment/moment.min.js',
]); */
// 安装组件
install('portal', index);
