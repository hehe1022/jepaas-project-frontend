/**
 * @Author : ZiQin Zhai
 * @Date : 2019/6/4 14:21
 * @Version : 1.0
 * @Last Modifined by : ZiQin Zhai
 * @Last Modifined time : 2019/6/4 14:21
 * @Description 菜单Class
 * */
class MenuModel {
  constructor({
    title, icon, isShow, path, isActive,
  }) {
    this.title = title;
    this.icon = icon;
    this.isShow = isShow;
    this.path = path;
    this.isActive = isActive;
  }

  /**
   * 实例被激活时触发
   */
  actived() {
    this.isActive = true;
  }

  /**
   * 失活
   */
  unActived() {
    this.isActive = false;
  }

  /**
   * 向当前对象中添加属性
   */
  setAttribute(key, val) {
    this[key] = val;
  }
}

class FuncMenu extends MenuModel {
  constructor({
    title, icon, isShow, path, isActive, id, code, type, tempTab, pluginid, pluginCode,
  }) {
    super({
      title,
      icon,
      isShow,
      path,
      isActive,
    });
    this.pluginCode = pluginCode;
    this.tempTab = '';
    this.id = id;
    this.code = code;
    this.type = type;
    this.pluginid = pluginid;
  }
}

/**
 * factory function
 * @param title 标题
 * @param icon 图标
 * @param isShow 是否显示
 * @param path 地址
 * @param isActive 是否被选中
 * @returns {MenuModel}
 */
function createMenu({
  title, icon, isShow, path, isActive,
}) {
  return new MenuModel(...arguments);
}

/**
 * factory function
 * @param title 标题
 * @param icon 图标
 * @param isShow 是否显示
 * @param path 路径
 * @param isActive 是否选中
 * @param id 功能id
 * @param code 功能编码
 * @param type 类型
 * @param tempTab
 * @param pluginid 插件id
 * @returns {FuncMenu}
 */
function createFuncMenu({
  title, icon, isShow, path, isActive, id, code, type, tempTab, pluginid, pluginCode,
}) {
  return new FuncMenu(...arguments);
}

export { createMenu, createFuncMenu };
