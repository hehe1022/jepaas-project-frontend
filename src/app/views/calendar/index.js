// import Vue from 'vue';
// import 'echarts/lib/component/legendScroll';
// import App from './index.vue';

// Vue.config.productionTip = false;
// Vue.config.devtools = true;

// new Vue({
//   // router,
//   // store,
//   render: h => h(App),
// }).$mount('#app');
import router from './router';
import config from './config.json';
import install from '../../util/install';

install(router, config);
