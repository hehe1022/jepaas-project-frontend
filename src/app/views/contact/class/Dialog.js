/* 会话
 * @Author: Qinyonglian
 * @Date: 2019-07-23 14:12:50
 * @LastEditors: Qinyonglian
 * @LastEditTime: 2019-07-23 14:16:57
 */
import {
  fetchCreateDialogId,
} from '../actions/dialog';

export default class Dialog {
  /*
     * 获取会话id
     * @param {*} friendUserId 朋友id
     * @param {*} userId 当前userId
     * @param {*} type 1/对话2/群组
     */
  static async createDialogId(friendUserId, userId, type) {
    const res = await fetchCreateDialogId({
      friendUserId,
      userId,
      type,
    });
    if (res.success) {
      return res.obj || {};
    }
    JE.msg(res.message);

    return {};
  }

  /*
    * 创建群聊会话
    * @param {} leftId
    * @param {*} rightId
    * @param {*} otherInfo
    */
  static async createGroupChat(leftId, rightId, otherInfo = {}) {
    const res = await this.createDialogId(rightId, leftId, '3');
    return {
      ...res,
      otherInfo,
      photo: otherInfo.photo || otherInfo.icon || '',
    };
  }
}
