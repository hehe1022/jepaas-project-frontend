/* 会话
 * @Author: Qinyonglian
 * @Date: 2019-07-23 13:36:42
 * @LastEditors: Qinyonglian
 * @LastEditTime: 2019-07-23 14:12:18
 */

import fetchAsync from '@/helper/httpUtil';
import { HOST_RBAC } from '../../../constants/config';

/*
 * 获取会话id
 * @param param
 * @returns {Promise<T | never>}
 */
// eslint-disable-next-line import/prefer-default-export
export function fetchCreateDialogId(param) {
  return fetchAsync(`${HOST_RBAC}/instant/instant/news/getDialogId`, {}, {
    type: 'post',
    data: param,
  })
    .then(data => data)
    .catch();
}
