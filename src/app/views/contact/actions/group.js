/*
 * @Author: Qinyonglian
 * @Date: 2019-06-12 09:51:32
 * @LastEditors: Qinyonglian
 * @LastEditTime: 2019-08-07 15:52:03
 */


import fetch from '@/helper/httpUtil';
import { HOST_RBAC } from '../../../constants/config';

/*
 * 获取我的群组
 * @param param
 * @returns {Promise<T | never>}
 */
// eslint-disable-next-line import/prefer-default-export
export function fetchMyGroups(param) {
  return fetch(`${HOST_RBAC}/instant/instant/group/getMyGroups`, {}, {
    type: 'post',
    data: param,
    // contentType: 'application/x-www-form-urlencoded',
  })
    .then(data => data)
    .catch();
}
