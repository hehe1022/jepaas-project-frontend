/* 组织树
 * @Author: Qinyonglian
 * @Date: 2019-07-23 13:36:42
 * @LastEditors: Qinyonglian
 * @LastEditTime: 2019-07-23 14:09:21
 */


import fetchAsync from '@/helper/httpUtil';
import { HOST_RBAC } from '../../../constants/config';

/*
 * 根据userId获取所有组织信息
 * @param param
 * @returns {Promise<T | never>}
 */
export function fetchGetAllOrg(param) {
  return fetchAsync(`${HOST_RBAC}/instant/instant/rbac/getAllOrg`, {}, {
    type: 'post',
    data: param,
    isFormSubmit: true,
  })
    .then(data => data)
    .catch();
}
/*
 * 根据id获取id下组织信息
 * @param param
 * @returns {Promise<T | never>}
 */
export function fetchGetOrg(param) {
  return fetchAsync(`${HOST_RBAC}/instant/instant/rbac/getOrgs`, {}, {
    type: 'post',
    data: param,
  })
    .then(data => data)
    .catch();
}

/*
 * 根据id获取部门详细信息
 * @param param
 * @returns {Promise<T | never>}
 */
export function fetchGetOrgById(param) {
  return fetchAsync(`${HOST_RBAC}/instant/instant/rbac/getOrgById`, {}, {
    type: 'post',
    data: param,
  })
    .then(data => data)
    .catch();
}

/*
 * 根据id获取用户详细信息
 * @param param
 * @returns {Promise<T | never>}
 */
export function fetchGetUserInfoById(param) {
  return fetchAsync(`${HOST_RBAC}/instant/instant/rbac/getUserInfoById`, {}, {
    isFormSubmit: true,
    type: 'post',
    data: param,
  })
    .then(data => data)
    .catch();
}

/*
 * 根据ids获取用户详细信息
 * @param param
 * @returns {Promise<T | never>}
 */
export function fetchGetUserInfoByIds(param) {
  return fetchAsync(`${HOST_RBAC}/instant/instant/rbac/getUserInfoByIds`, {}, {
    type: 'post',
    data: param,
    isFormSubmit: true,
  })
    .then(data => data)
    .catch();
}

/*
 * 根据组织id获取所有人员信息
 * @param param
 * @returns {Promise<T | never>}
 */
export function fetchGetUserByOrgId(param) {
  return fetchAsync(`${HOST_RBAC}/instant/instant/rbac/getUserByOrgId`, {}, {
    type: 'post',
    data: param,
  })
    .then(data => data)
    .catch();
}

/*
 * 根据租户ID获取这个租户下的所有人员
 * @param param
 * @returns {Promise<T | never>}
 */
export function fetchGetAllPeople(param) {
  return fetchAsync(`${HOST_RBAC}/instant/instant/rbac/getAllUserInfosByzhId`, {}, {
    type: 'post',
    data: param,
    isFormSubmit: true,
  })
    .then(data => data)
    .catch();
}
